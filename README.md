# https://youtu.be/Z9G5stlXoyg

https://youtu.be/Z9G5stlXoyg

# CDD_Web 
 
# Simple registration and login system.

    ( NOTE : This project will create empty "users.txt" file in C_Drive -- C:\AAA\users.txt (or) /opt/AAA/users.txt -- using file I/O handling as backend )

    1. At the first user need to register. It will get stored in users.txt  

    2. After the registration, user can login

    3. In this project, there are 6 pages
	  	i - Login page
	       ii - Register page
	      iii - Error page
	       iv - Welcome page
	  	v - Content page
	       vi - About us

    4. In here.. Welcome page, Content page are secured page. These are not accessible by the URLs without logging in.
       If any user tries to login by those URLs directly, it will fall back to Login_Page and forces to login.

# Servlets || JSPs

    ( NOTE : Deploy it on any server )

    WORK FLOWs 

    1. Registration process

	  LoginPage.jsp (or) RegisterPage.jsp --> Form submition --> RegisterServlet.java --> FileHandling.java
	  We can access directly either RegisterPage.jsp or LoginPage.jsp to register for the first time.
	  On clicking the submit button, "RegisterServlet.java" servlet will get called. Instance object for FileHangling.java will be created here. This will write and stores the credentials in "users.txt" file.

    2. Login and checking credentials

	  LoginPage.jsp --> ValidateServlet.java --> FileHanling.java --> Returns boolean value to ValidateServlet.java
	  If false --> ErrorPage.jsp
	  If true  --> WelcomePage.jsp --> ContentPage.jsp --> AboutUs.jsp
		       Credentials will get stored in Session_Storage, to maintain the page redirection.

    3. Logout Functionality 
	  
	  Logout button --> LogoutServlet.java
	  It will remove the credentials from the Session_Storage and will terminate the session.

# CI tool : Jenkins || CD tool : CDDirector -- CDD

	OVERVIEW

	Jenkins will receive the push notification from our SCMtool (GitLab).
	Jenkins will create a build. After the successful build, BUILD_NOTIFICATION will be sent to CDD.
	CDD will receive the BUILD_NOTIFICATION and creates a release.

	-------------------------------------------------------------------------------------------------------------------------------------------------

	JENKINS JOB

	Source Code Management :   Git url -- https://gitlab.com/wiprolimited2/cdd_web.git
	Build Triggers 	       :   Build when a change is pushed to GitLab. GitLab webhook URL: https://jenkins.wiprodigitalrig.com/project/CDDWeb
	Build 		       :   mvn clean deploy
	Post-build Actions     :   Send notification to CDDirector
				   Override CDDirector configuration
 				   CDDirector Server Name          : cddirector.io	
 				   CDDirector Server Port          : 443
 	                           Is CDDirector Secured Communication?	YES
 				   Tenant ID (copy from CDD user settings) will be like fc200001-5aa0-44ca-575z-731a2b90e456
 				   API Key (copy from CDD user settings) it will be concealed here.
				   Set Application Name            : Use Source Code Repository Name as Application Name
				   Set Application Version Name    : Use Source Code Branch Name as Application Version
				   Set Application Source Name     : Use Repository Namespace as the Application Source
				   The notification should trigger : Execute an existing release

	-------------------------------------------------------------------------------------------------------------------------------------------------

	CDD's RELEASE 

	Release name    : CDDWeb
	Release version : 1.0
	Applications    : CDD_Web(1.0.0)
	No. of phases   : 2
			  Phase 1 - QA 
			  No. of tasks : 3
				Task 1 - Deploy CDDWeb to QA   -- application will be deployed to the Quality_Assurance environment.
				Task 2 - Sonar_Analysis        -- a kind of gateway check, to check whether the application meet with the requirements.
				Task 3 - Run QA Tests	       -- set of test will be run which are necessary for QA environment.
			  Phase 2 - Prod
			  No. of tasks : 3
				Task 1 - Deploy CDDWeb to Prod -- application will be deployed to the Production environment.
				Task 2 - Sonar_Analysis        -- a kind of gateway check, to check whether the application meet with the requirements.
				Task 3 - Run Prod Tests	       -- set of test will be run which are necessary for Production environment.

	
	-------------------------------------------------------------------------------------------------------------------------------------------------
