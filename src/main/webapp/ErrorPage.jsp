<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Error page</title>
</head>
<body bgcolor="red">
	<%
		response.setHeader("Cache-Control","no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Expires","0");
	%>
	<center><h2>Sorry, invalid credentials.</h2></center>
	<br><center>(or)</center><br>
	<center><h2>Please register, if not</h2></center>
	<br>
	<p><h4>Go back to login page </h4><a href="LoginPage.jsp">Click here</a></p>
</body>
</html>
