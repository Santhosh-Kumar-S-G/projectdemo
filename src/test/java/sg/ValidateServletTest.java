package sg;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ValidateServletTest extends TestCase {
    public ValidateServletTest( String testName )
    {
        super( testName );
    }
    public static Test suite()
    {
        return new TestSuite( ValidateServletTest.class );
    }
	public void testApp()//UNIT_TEST_CASE_FOR_PASSWORD
    {
        try {
			//assertTrue( true );
			System.out.println("called passwordTesting.java");
			JunitTesting test=new JunitTesting();
			ValidateServlet vs=new ValidateServlet();			String pw=vs.pw;
			boolean output;
			if(pw!=null)
				output=test.lenCheck(vs.pw);
			else
				output=test.lenCheck("********");
			assertEquals(true,output);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	public void testApp1() //UNIT_TEST_CASE_FOR_USERNAME
    {
        try {
			//assertTrue( true );
			System.out.println("called usernameTesting.java");
			JunitTesting test=new JunitTesting();
			ValidateServlet vs=new ValidateServlet();
			String uname=vs.uname;
			boolean output;
			if(uname!=null)
				output=test.lenCheck(vs.uname);
			else
				output=test.lenCheck("********");
			assertEquals(true,output);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	public void testApp2()
	{
		try {
			System.out.println("called usernameFinder");
			JunitTesting test=new JunitTesting();
			ValidateServlet vs=new ValidateServlet();
			boolean output;
			if(vs.uname==null)
				output=true;
			else
				output=test.unameCheck(vs.uname);
			assertEquals(true,output);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void testApp3()
	{
		try {
			System.out.println("called passwordFinder");
			JunitTesting test=new JunitTesting();
			ValidateServlet vs=new ValidateServlet();
			boolean output;
			if(vs.pw==null)
				output=true;
			else
				output=test.pwdCheck(vs.pw);
			assertEquals(true,output);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
