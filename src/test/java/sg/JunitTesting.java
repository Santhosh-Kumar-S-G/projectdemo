package sg;

public class JunitTesting {
	public boolean lenCheck(String s) {
		System.out.println("called string count");
		int c=0;
		for(char ch:s.toCharArray())
			c++;
		if(c!=0)
			return true;
		return false;
	}
	public boolean unameCheck(String s) {
		System.out.println("called username finder");
		int c=0;
		for(String x:FileHandline.usernames)
			if(s.equalsIgnoreCase(x))
				c=1;
		if(c==1)
			return true;
		return false;
	}
	public boolean pwdCheck(String s) {
		System.out.println("called password finder");
		int c=0;
		for(String x:FileHandline.passwords)
			if(s.equals(x))
				c=1;
		if(c==1)
			return true;
		return false;
	}
}